package characters;

import dodge.DodgeImpl;

public interface Crashable {

	void collides(final DodgeImpl dodge);

}
