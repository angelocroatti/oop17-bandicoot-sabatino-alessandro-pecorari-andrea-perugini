package characters;

public interface Character {
	
	Position getPosition();
	
	void move();
	
}
