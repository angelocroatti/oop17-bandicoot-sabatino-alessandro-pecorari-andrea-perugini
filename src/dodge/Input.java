package dodge;

public interface Input {

    void addListeners();
    void removeListeners();
    boolean isMoveUp();
    boolean isMoveDown();
	
}
